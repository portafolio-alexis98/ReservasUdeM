## Reservas UdeM

Prototipo de aplicacion movil para realizar reserva de aulas y la apertura de la misma mediante codigo QR.

## Tecnologias usadas

AngularJS, Ionic, Cordova, Firebase.

## Screenshots

![alt text](https://gitlab.com/portafolio-alexis98/ReservasUdeM/raw/master/screenshots/1.jpg)
![alt text](https://gitlab.com/portafolio-alexis98/ReservasUdeM/raw/master/screenshots/2.jpg)
![alt text](https://gitlab.com/portafolio-alexis98/ReservasUdeM/raw/master/screenshots/3.jpg)
![alt text](https://gitlab.com/portafolio-alexis98/ReservasUdeM/raw/master/screenshots/4.jpg)