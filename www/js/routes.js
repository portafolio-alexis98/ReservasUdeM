angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    

      .state('menu.inicio', {
    url: '/page1',
    views: {
      'side-menu21': {
        templateUrl: 'templates/inicio.html',
        controller: 'inicioCtrl'
      }
    }
  })

  .state('menu', {
    url: '/side-menu21',
    templateUrl: 'templates/menu.html',
    controller: 'menuCtrl'
  })

  .state('menu.reservar', {
    url: '/page2',
    views: {
      'side-menu21': {
        templateUrl: 'templates/reservar.html',
        controller: 'reservarCtrl'
      }
    }
  })

  .state('menu.misReservas', {
    url: '/page3',
    views: {
      'side-menu21': {
        templateUrl: 'templates/misReservas.html',
        controller: 'misReservasCtrl'
      }
    }
  })

  .state('menu.abrirAula', {
    url: '/page4',
    views: {
      'side-menu21': {
        templateUrl: 'templates/abrirAula.html',
        controller: 'abrirAulaCtrl'
      }
    }
  })
    .state('nuevareserva', {
      url: '/nuevareserva',
      templateUrl: 'templates/nueva.html',
      controller: 'nuevaReservaCtrl',
      cache: false
  })


$urlRouterProvider.otherwise('/side-menu21/page1')


});