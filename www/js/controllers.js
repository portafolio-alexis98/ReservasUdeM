angular.module('app.controllers', [])
.controller('inicioCtrl', ['$scope', '$state','$stateParams', '$firebaseAuth',
function ($scope, $state,$stateParams, $firebaseAuth) { 
    $scope.datos = {};
    $scope.authObj = $firebaseAuth();
    $scope.resultado = "";

    $scope.iniciarSesion = function () {
        console.log($scope.datos.correo);
        $scope.authObj.$signInWithEmailAndPassword($scope.datos.correo, $scope.datos.password)
        .then(function(firebaseUser) {
            console.log("Signed in as:", firebaseUser.uid);
            $state.go('menu.reservar');
        }).catch(function(error) {
            console.error("Authentication failed:", error);
        });

    };

    
}])
.controller('menuCtrl', ['$scope', '$stateParams', 
function ($scope, $stateParams) {


}])
.controller('reservarCtrl', 
    ['$scope', '$state', '$stateParams', 'MyService', 
function ($scope, $state, $stateParams, MyService) {
    $scope.cursos = [{
        nombre: 'Sistemas digitales',
        horario: 'Miercoles-Jueves 6-8',
        alumnos: 14
    },{
        nombre: 'Ciberseguridad',
        horario: 'Miercoles-Jueves 6-8',
        alumnos: 20
    },{
        nombre: 'Software 3',
        horario: 'Martes-Jueves 10-12',
        alumnos: 26
    }]
    
    $scope.hacerReserva = function (elCurso) {
        MyService.info.curso = elCurso;
        $state.go('nuevareserva');
    }
}])
.controller('misReservasCtrl', ['$scope', '$stateParams', '$firebaseArray', 
function ($scope, $stateParams, $firebaseArray) {
    var ref = firebase.database().ref().child("reservas");
    $scope.reservas = $firebaseArray(ref);
}])   
.controller('abrirAulaCtrl', ['$scope', '$stateParams', '$cordovaBarcodeScanner', '$firebaseArray', 
function ($scope, $stateParams, $cordovaBarcodeScanner, $firebaseArray) {
    var ref = firebase.database().ref().child("salones");
    $scope.salones = $firebaseArray(ref);

    $scope.datos = {};

    $scope.escanear = function () {
        $cordovaBarcodeScanner.scan(
      function (result) {
            $scope.datos.salonseleccionado = result.text;

          alert("We got a barcode\n" +
                "Result: " + result.text + "\n" +
                "Format: " + result.format + "\n" +
                "Cancelled: " + result.cancelled);

            var estado = $scope.salones.$getRecord($scope.datos.salonseleccionado);
            console.log(estado);
            estado = true;
            console.log(estado);
            $scope.salones.$save(estado);
      },
      function (error) {
          alert("Scanning failed: " + error);
      },
      {
          preferFrontCamera : true, // iOS and Android
          showFlipCameraButton : true, // iOS and Android
          showTorchButton : true, // iOS and Android
          torchOn: true, // Android, launch with the torch switched on (if available)
          saveHistory: true, // Android, save scan history (default false)
          prompt : "Apunte al QR ubicado en la puerta", // Android
          resultDisplayDuration: 500, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
          formats : "QR_CODE,PDF_417", // default: all but PDF_417 and RSS_EXPANDED
          orientation : "landscape", // Android only (portrait|landscape), default unset so it rotates with the device
          disableAnimations : true, // iOS
          disableSuccessBeep: false // iOS and Android
      }
   );};

}])
.controller('nuevaReservaCtrl', 
    ['$scope', '$stateParams', '$cordovaDatePicker', 'MyService', '$firebaseObject', '$firebaseArray', '$cordovaLocalNotification',
function ($scope, $stateParams, $cordovaDatePicker, MyService, $firebaseObject, $firebaseArray, $cordovaLocalNotification) {
    var ref = firebase.database().ref().child("reservas");
    $scope.reservas = $firebaseArray(ref);
    $scope.miCurso = MyService.info.curso;
    $scope.fecha = "10/05/2016"
    $scope.hora = "8:00"
    $scope.equipos = {
        aire: false,
        proyector: false,
        pc: false
    }

    $scope.reservaGuardada = false;

    $scope.guardarReserva = function () {
        $scope.reservas.$add({
            curso: $scope.miCurso,
            fecha: $scope.fecha,
            hora: $scope.hora,
            equipos: $scope.equipos
        });
        $scope.reservaGuardada = true;

        cordova.plugins.notification.local.schedule({
            title: 'Recuerda tu reserva!',
            text: 'Fecha:'+ $scope.fecha + ', Hora: '+$scope.hora,
            trigger: { in: 30, unit: 'second' }
        });
    };

    function setDate(date) {
        $scope.fecha = formatDate(date);
        setDate(date)
    }

    function setTime(time) {
        $scope.hora = formatTime(time)
        setTime(time)
    }
    
    function onError(error) { // Android only
        //alert('Error: ' + error);
    }

    $scope.selectFecha = function () {
        var options = {
            date: new Date(),
            mode: 'date', 
            minDate: new Date() - 10000,
            androidTheme: 5,
            titleText: 'Seleccionar fecha:',
            okText: 'Aceptar',
            cancelText: 'Cancelar',
        };
        datePicker.show(options, setDate, onError);
        console.log("HOLA MUNDO")
    }

    $scope.selectHora = function () {
        var options = {
            date: new Date(),
            mode: 'time', 
            minDate: new Date() - 10000,
            androidTheme: 5,
            titleText: 'Seleccionar Hora:',
            okText: 'Aceptar',
            cancelText: 'Cancelar',
            is24Hour: true,
        };   
        datePicker.show(options, setTime, onError);
        console.log("HOLA MUNDO")
    }

    function formatDate(date) {
        var monthNames = [
        "Enero", "Febrero", "Marzo",
        "Abril", "Mayo", "Junio", "Julio",
        "Agosto", "Septiembre", "Octubre",
        "Noviembre", "Diciembre"
        ];

        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();

        return day + ' ' + monthNames[monthIndex] + ' ' + year;}

    function formatTime(time) {
        var hour = time.getHours();
        var minutes = time.getMinutes();
        return hour + ':' + minutes + " H";
    }
}])
 